# Server Curio Fabric

![GitHub](https://img.shields.com.servercurio.fabric.security.io/github/license/servercurio/fabric?style=for-the-badge)
![CircleCI](https://img.shields.com.servercurio.fabric.security.io/circleci/build/github/servercurio/fabric/master?style=for-the-badge)
![Sonar Coverage](https://img.shields.com.servercurio.fabric.security.io/sonar/coverage/com.servercurio.fabric:fabric?server=https%3A%2F%2Fsonarcloud.com.servercurio.fabric.security.io&sonarVersion=8.0&style=for-the-badge)
![Sonar Tests](https://img.shields.com.servercurio.fabric.security.io/sonar/tests/com.servercurio.fabric:fabric?server=https%3A%2F%2Fsonarcloud.com.servercurio.fabric.security.io&sonarVersion=8.0&style=for-the-badge)
![Sonar Quality Gate](https://img.shields.com.servercurio.fabric.security.io/sonar/quality_gate/com.servercurio.fabric:fabric?server=https%3A%2F%2Fsonarcloud.com.servercurio.fabric.security.io&sonarVersion=8.0&style=for-the-badge)

An application development framework.